package com.example.lab5task2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText area;
    EditText address;
    EditText addition;
    ListView list;
    ArrayList<String> items = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        area = findViewById(R.id.area);
        address = findViewById(R.id.address);
        addition = findViewById(R.id.addition);
        list = findViewById(R.id.list);
    }
    public void onClick(View v) {
        Integer index = items.size()+1;
        items.add(
                index.toString() + ". "+ address.getText()+
                        "; " + area.getText() + "m^2; " + addition.getText()
                );
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, items);
        list.setAdapter(adapter);
        area.setText(null);
        address.setText(null);
        addition.setText(null);
    }
}
