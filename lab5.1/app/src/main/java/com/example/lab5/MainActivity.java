package com.example.lab5;

import android.os.Bundle;

import android.widget.*;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import com.example.lab5.databinding.ActivityMainBinding;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    TextView tvInfo;
    EditText etInput;
    Button bControl;
    int r;
    Random rnd = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        tvInfo = (TextView)findViewById(R.id.result);
        etInput = (EditText)findViewById(R.id.textInput);
        bControl = (Button)findViewById(R.id.button);
        r = rnd.nextInt(100)+1;
    }

    public void onClick(View v){
        int ch = Integer.parseInt(etInput.getText().toString());
        if (ch > r) tvInfo.setText(getResources().getText(R.string.ahead));
        else if (ch < r) tvInfo.setText(getResources().getText(R.string.behind));
        else {
            tvInfo.setText(getResources().getText(R.string.hit));
            r = rnd.nextInt(100)+1;
        }
    }
}